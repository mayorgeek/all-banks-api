package com.aiotouch.allbanks;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AllBanksApplication {

	public static void main(String[] args) {
		SpringApplication.run(AllBanksApplication.class, args);
	}

}
