package com.aiotouch.allbanks.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.aiotouch.allbanks.model.Bank;
import com.aiotouch.allbanks.service.BankService;

@RestController
@RequestMapping("/api/banks")
public class BankController {

	private BankService bankService;
	
	public BankController(BankService bankService) {
		super();
		this.bankService = bankService;
	}

	// Add New Bank To Database
	@PostMapping()
	public ResponseEntity<Bank> saveBank(@RequestBody Bank bank) {
		return new ResponseEntity<Bank>(bankService.save(bank), HttpStatus.CREATED);
	}
	
	
	// Get All Banks In The World
	@GetMapping()
	public List<Bank> getAllBanks() {
		return bankService.getAllBanks();
	}
	
	
	// Get Bank By Country Code
	@GetMapping("{countryCode}")
	public ResponseEntity<Bank> getBanksByCountryCode(@RequestParam(name = "countryCode") String countryCode) {
		System.out.println("working");
		return new ResponseEntity<Bank>(bankService.getBanksByCountryCode(countryCode), HttpStatus.OK);
	}
	
	
	// Get Bank By Country Name
	@GetMapping("{countryName}")
	public ResponseEntity<Bank> getBanksByCountryName(@RequestParam(name = "countryName") String countryName) {
		return new ResponseEntity<Bank>(bankService.getBanksByCountryName(countryName), HttpStatus.OK);
	}
	
	
	// Update Bank Details
	@PutMapping()
	public Bank updateBankDetails() {
		return null;
	}
	
	
	// Delete Bank
	@DeleteMapping()
	public ResponseEntity<Bank> deleteBank() {
		return null;
	}
	
	
	
	
	
}
